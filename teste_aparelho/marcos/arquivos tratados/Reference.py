import pandas as pd
class referencia(object):
    def m_eletrodos(self,df):
        ele_ret=['A1','A2','FP1','FP2','CZ','F7','F8','PZ','EX1','EX2','EX3','EX4','EX5','EX6'] #lista de eletrodos retirados
        ele_sel=['T6', 'O2', 'FZ','C3','C4', 'T5','P4', 'O1', 'F3', 'OZ', 'F4','T3','T4', 'P3']#lista de eletrodos selecionados
        N=len(ele_sel)#número de eletrodos
        med=len(df)*[0]
        df2=df.copy()
        columns=len(df.columns)
        lines=len(df)
        for i in range(columns):#retira elementos que nao serao referenciados
                for l in range(len(ele_ret)):
                    if df.columns[i]==ele_ret[l]:
                        df2.pop(ele_ret[l])
        for i in range(lines):
            med[i]=(sum(df2.iloc[i,:])/N)#lista com as medias das linhas
        for i in range(columns):
            if df.columns[i] in ele_sel:
                df.iloc[:,i]=df.iloc[:,i]-med
        return  df
    def cz(self,df):
        ele_sel=['FP1','FP2','F7','F8','F3','FZ','F4','T3','C3','C4','T4','P3','PZ','P4','T5','T6','O1','O2']
        for i in range(len(df.columns)):
            for l in range(len(ele_sel)):
                if df.columns[i]==ele_sel[l]:
                    df.iloc[:,i]=df.iloc[:,i]-df.CZ
        return df
    def A1_A2(self,df):
        ele_sel_A1=['F7','T3','T5','FP1','F3','C3','P3','O1','FP2','F4','C4']
        ele_sel_A2=['F8','T4','T6','FP2','F4','C4','P4','O2']
        for i in range(len(df.columns)):#retira das medidas os valores de A1 para os ele_sel_A1
            for l in range(len(ele_sel_A1)):
                if df.columns[i]==ele_sel_A1[l]:
                    df.iloc[:,i]=df.iloc[:,i]-df.A1
        for i in range(len(df.columns)):#retira das medidas os valores de A2 para os ele_sel_A2
            for l in range(len(ele_sel_A2)):
                if df.columns[i]==ele_sel_A2[l]:
                    df.iloc[:,i]=df.iloc[:,i]-df.A2
        return df
    def med_A1_A2(self,df):
        ele_med_A1_A2=(df.A1+df.A2)/2 #media das medições de A1 e A2
        ele_sel=['FP1','FP2','F7','F8','F3','FZ','F4','T3','C3','C4','T4','P3','PZ','P4','T5','T6','O1','O2','CZ']
        for i in range(len(df.columns)):
            for l in range(len(ele_sel)):
                if df.columns[i]==ele_sel[l]:
                    df.iloc[:,i]=df.iloc[:,i]-ele_med
        return df
