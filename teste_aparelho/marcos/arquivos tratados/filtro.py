from scipy import signal
import numpy as np
import pandas as pd
class filtro(object):
    def filt60(self,df):
        df=df-df.mean()
        lowcut = 59
        highcut = 61
        order = 4
        nyq = 0.5 * 512 #512 hz é a frequência de amostra
        low = lowcut / nyq
        high = highcut / nyq
        b, a = signal.butter(order, [low, high], btype='bandstop')
        df_filtro=df.copy()
        for i in range(len(df.columns)):
            df.iloc[:,i]= signal.filtfilt(b, a, df.iloc[:,i])

        return df
    def bandpasseeg(self,df):
        lowcut = 1
        highcut = 20
        order = 4
        nyq = 0.5 * 512
        low = lowcut / nyq
        high = highcut / nyq
        b, a = signal.butter(order, [low, high], btype='bandpass')
        for i in range(len(df.columns)):
            df.iloc[:,i]= signal.filtfilt(b, a, df.iloc[:,i])
        return df
