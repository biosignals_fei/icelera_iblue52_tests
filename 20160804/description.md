# Estudo dos sinais de EMG do iBlue52

## EMG

Os arquivos .dat tem a montagem dos eletrodos conforme o nome do arquivo:

* mclaudia_emg_rightarm.dat
* mclaudia_emg_rightarm4.dat
* mclaudia_emg_rightarm_EX2.dat 
* mclaudia_emg_rightarm_EX4.dat 
* mclaudia_emg_rightarm_EX5.dat 
* mclaudia_emg_rightarm_EX6.dat 

## EEG + EMG

Estes arquivos possuem uma montagem dos sinais de EMG, com os eletrodos ao redor do olho e EEG obtido com a touca fornecida pela iCelera.

* thiago_eeg-emg_20160808.dat
* thiago_eeg-emg_20160808_1.dat
* thiago_eeg-emg_20160808_2.dat

## Impressão e FFT

Arquivo que processa os dados em csv, após processamento com o icelera.py

* processing.py