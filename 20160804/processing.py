import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


fname = './mclaudia_emg_rightarm_EX6.dat.csv'
df = pd.read_csv(fname)
df.EX6.plot()
plt.savefig(fname+'.png')
plt.close()

Y = np.fft.fft(df.EX6, df.EX6.size)
freq = np.fft.fftfreq(df.EX6.size, 1/512)

plt.plot(freq[1:df.EX6.size/2], Y.real[1:df.EX6.size/2])
plt.savefig(fname+'.fft.png')
plt.close()
