# Descrição

Simulações e testes feitos com iCelera - iBlue ao longo do desenvolvimento do software de integração:

https://bitbucket.org/rodrigoprior/icelera_iblue52_integration/

## Observações

Os arquivos .dat são compatíveis apenas com a versão 0.0.1 (tag) do software.

Nesse caso antes de executar o script é preciso fazer checkout na tag:

    $ git clone https://bitbucket.org/rodrigoprior/icelera_iblue52_integration/
    $ cd icelera_iblue52_integration
	$ git checkout 0.0.1
	$ python3 icelera.py csv /<path_to_dat_files>/*.dat